Yet Another Inkscape Plugin for GCode (yaip4g)
==============================================

This plugin is is fully based on the great [J-Tech Laser Tool][jtp] plugin for
Inkscape.

The reason for it's existance is to add support for laser power by setting the
stroke color. This means that for a Red line could be to cut (cut power and
speed), a Blue line may be for engraving (less power and maybe more speed),
etc.

A future option may also be to add a materials database to allow you to
configure cutting/engraving profiles per type of material. But this is future.



<!-- Links -->
[jtp]: https://jtechphotonics.com/?page_id=1980
